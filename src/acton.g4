grammar acton;

@headers {
//package main.parsers;
    import main.* ;
//    import main.ast.* ;
    import main.ast.node.* ;
    import main.ast.node.declaration.* ;
    import main.ast.node.declaration.handler.* ;
    import main.ast.node.expression.* ;
    import main.ast.node.expression.operators.* ;
    import main.ast.node.expression.values.* ;
    import main.ast.node.statement.* ;
    import main.ast.type.* ;
    import main.ast.type.actorType.* ;
    import main.ast.type.arrayType.* ;
    import main.ast.type.primitiveType.* ;
    import java.util.ArrayList ;
}

program returns [Program p]
    : {
        $p = new Program();
    }
    ( actorDec=actorDeclaration {
            $p.addActor($actorDec.ad);
        }
    )+ progMain=mainDeclaration {
        $p.setMain($progMain.md);
        }
    ;

actorDeclaration returns [ActorDeclaration ad]
    :   ACTOR actorNameDec=identifier {
            $ad = new ActorDeclaration($actorNameDec.id);
            $ad.setLine($actorDec.getLine());
        }
        (EXTENDS parName=identifier {
            $ad.setParentName($parName.id);
        })? LPAREN sz=INTVAL {
            $ad.setQueueSize($sz);
        } RPAREN
        LBRACE

        (KNOWNACTORS
        LBRACE
            (kaType=identifier kaName=identifier {
                kat = new ActorType($kaType.id);
                kat.setLine($kaType.getLine());
                kaVar = new VarDeclaration($kaName.id, kat);
                kaVar.setLine($kaName.getLine());
                $ad.addKnownActor(kaVar);
            } SEMICOLON)*
        RBRACE)

        (ACTORVARS
        LBRACE
            actorVars=varDeclarations {
                $ad.setActorVars($actorVars.alvd);
            }
        RBRACE)

        (ihd=initHandlerDeclaration {
            $ad.setInitHandler($ihd.ihdec);
        })?
        (mhd=msgHandlerDeclaration {
            $ad.addMsgHandler($mhd.mhdec);
        })*

        RBRACE
    ;

mainDeclaration returns [Main md]
    :   MAIN
    	LBRACE
        actorInstantiation*
    	RBRACE
    ;

actorInstantiation
    :	identifier identifier
     	LPAREN (identifier(COMMA identifier)* | ) RPAREN
     	COLON LPAREN expressionList RPAREN SEMICOLON
    ;

initHandlerDeclaration returns [InitHandlerDeclaration ihdec]
    :	MSGHANDLER ihName=INITIAL {
        ihid = new Identifier($ihName.text);
        $ihdec = new InitHandlerDeclaration(ihid);
    } LPAREN ads=argDeclarations {
        $ihdec.setArgs($ads.alvd);
    } RPAREN
     	LBRACE
     	varDeclarations
     	(statement)*
     	RBRACE
    ;

msgHandlerDeclaration returns [MsgHandlerDeclaration mhdec]
    :	MSGHANDLER mhName=identifier {
        $mhdec = new MsgHandlerDeclaration($mhName.id);
    } LPAREN argDeclarations RPAREN
       	LBRACE
       	varDeclarations
       	(statement)*
       	RBRACE
    ;

argDeclarations returns [ArrayList<VarDeclaration> alvd]
    : {
        $alvd = new ArrayList<VarDeclaration>();
    }	vd=varDeclaration {
        $alvd.add($vd.vd);
    } (COMMA vds=varDeclaration {
        $alvd.add($vds.vd);
    } )* |
    ;

varDeclarations returns [ArrayList<VarDeclaration> alvd]
    :   {
        $alvd = new ArrayList<VarDeclaration>();
    }
    	(vds=varDeclaration {
    	    $alvd.add($vds.vd);
    	} SEMICOLON)*
    ;

varDeclaration returns [VarDeclaration vd]
    :	INT intvd=identifier {
        vdt = new IntType();
        vdn = new Identifier($intvd.text);
        $vd = new VarDeclaration(vdn,vdt);
    }
    |   STRING strvd=identifier {
        vdt = new StringType();
        vdn = new Identifier($strvd.text);
        $vd = new VarDeclaration(vdn,vdt);
    }
    |   BOOLEAN boolvd=identifier {
        vdt = new BoolType();
        vdn = new Identifier($boolvd.text);
        $vd = new VarDeclaration(vdn,vdt);
    }
    |   INT arrvd=identifier LBRACKET sz=INTVAL RBRACKET {
        vdt = new ArrayType($sz);
        vdn = new Identifier($arrvd.text);
        $vd = new VarDeclaration(vdn,vdt);
    }
    ;

statement
    :	blockStmt
    | 	printStmt
    |  	assignStmt
    |  	forStmt
    |  	ifStmt
    |  	continueStmt
    |  	breakStmt
    |  	msgHandlerCall
    ;

blockStmt
    : 	LBRACE (statement)* RBRACE
    ;

printStmt
    : 	PRINT LPAREN expression RPAREN SEMICOLON
    ;

assignStmt
    :    assignment SEMICOLON
    ;

assignment
    :   orExpression ASSIGN expression
    ;

forStmt
    : 	FOR LPAREN (assignment)? SEMICOLON (expression)? SEMICOLON (assignment)? RPAREN statement
    ;

ifStmt
    :   IF LPAREN expression RPAREN statement elseStmt
    ;

elseStmt
    : ELSE statement |
    ;

continueStmt
    : 	CONTINUE SEMICOLON
    ;

breakStmt
    : 	BREAK SEMICOLON
    ;

msgHandlerCall
    :   (identifier | SELF | SENDER) DOT
        identifier LPAREN expressionList RPAREN SEMICOLON
    ;

expression
    :	orExpression (ASSIGN expression)?
    ;

orExpression
    :	andExpression (OR andExpression)*
    ;

andExpression
    :	equalityExpression (AND equalityExpression)*
    ;

equalityExpression
    :	relationalExpression ( (EQ | NEQ) relationalExpression)*
    ;

relationalExpression
    : additiveExpression ((LT | GT) additiveExpression)*
    ;

additiveExpression
    : multiplicativeExpression ((PLUS | MINUS) multiplicativeExpression)*
    ;

multiplicativeExpression
    : preUnaryExpression ((MULT | DIV | PERCENT) preUnaryExpression)*
    ;

preUnaryExpression
    :   NOT preUnaryExpression
    |   MINUS preUnaryExpression
    |   PLUSPLUS preUnaryExpression
    |   MINUSMINUS preUnaryExpression
    |   postUnaryExpression
    ;

postUnaryExpression
    :   otherExpression (postUnaryOp)?
    ;

postUnaryOp
    :	PLUSPLUS | MINUSMINUS
    ;

otherExpression
    :    LPAREN expression RPAREN
    |    identifier
    |    arrayCall
    |    actorVarAccess
    |    value
    |    SENDER
    ;

arrayCall
    :   (identifier | actorVarAccess) LBRACKET expression RBRACKET
    ;

actorVarAccess
    :   SELF DOT identifier
    ;

expressionList
    :	(expression(COMMA expression)* | )
    ;

identifier returns [Identifier id]
    :   name=IDENTIFIER {
        $id = new Identifier($name.text);
    }
    ;

value
    :   INTVAL | STRINGVAL | TRUE | FALSE
    ;

// values
INTVAL
    : [1-9][0-9]* | [0]
    ;

STRINGVAL
    : '"'~["]*'"'
    ;

TRUE
    :   'true'
    ;

FALSE
    :   'false'
    ;

//types
INT
    : 'int'
    ;

BOOLEAN
    : 'boolean'
    ;

STRING
    : 'string'
    ;

//keywords
ACTOR
	:	'actor'
	;

EXTENDS
	:	'extends'
	;

ACTORVARS
	:	'actorvars'
	;

KNOWNACTORS
	:	'knownactors'
	;

INITIAL
    :   'initial'
    ;

MSGHANDLER
	: 	'msghandler'
	;

SENDER
    :   'sender'
    ;

SELF
    :   'self'
    ;

MAIN
	:	'main'
	;

FOR
    :   'for'
    ;

CONTINUE
    :   'continue'
    ;

BREAK
    :   'break'
    ;

IF
    :   'if'
    ;

ELSE
    :   'else'
    ;

PRINT
    :   'print'
    ;

//symbols
LPAREN
    :   '('
    ;

RPAREN
    :   ')'
    ;

LBRACE
    :   '{'
    ;

RBRACE
    :   '}'
    ;

LBRACKET
    :   '['
    ;

RBRACKET
    :   ']'
    ;

COLON
    :   ':'
    ;

SEMICOLON
    :   ';'
    ;

COMMA
    :   ','
    ;

DOT
    :   '.'
    ;

//operators
ASSIGN
    :   '='
    ;

EQ
    :   '=='
    ;

NEQ
    :   '!='
    ;

GT
    :   '>'
    ;

LT
    :   '<'
    ;

PLUSPLUS
    :   '++'
    ;

MINUSMINUS
    :   '--'
    ;

PLUS
    :   '+'
    ;

MINUS
    :   '-'
    ;

MULT
    :   '*'
    ;

DIV
    :   '/'
    ;

PERCENT
    :   '%'
    ;

NOT
    :   '!'
    ;

AND
    :   '&&'
    ;

OR
    :   '||'
    ;

QUES
    :   '?'
    ;

IDENTIFIER
    :   [a-zA-Z_][a-zA-Z0-9_]*
    ;

COMMENT
    :   '//' ~[\n\r]* -> skip
    ;

WHITESPACE
    :   [ \t\r\n] -> skip
    ;